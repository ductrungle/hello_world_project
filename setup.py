from setuptools import setup

requirements = [
    # package requirements go here
]

setup(
    name='hello_world_repo',
    version="0.0.2",
    description="Test deployment on gitlab",
    license="BSD",
    author="trung le",
    author_email='duc-trung.le@safrangroup.com',
    url='https://github.com/Destination github org or username/hello_world_repo',
    packages=['hello_world_repo'],
    
    install_requires=requirements,
    keywords='hello_world_repo',
    classifiers=[
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ]
)
