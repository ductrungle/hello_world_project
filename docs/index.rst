CoSApp documentation
====================

.. only:: html

   :Release: |release|
   :Date: |today|



Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

